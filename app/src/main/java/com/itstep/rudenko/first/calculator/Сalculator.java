package com.itstep.rudenko.first.calculator;

/**
 * Created by Evgen on 5/31/2016.
 */
public class Сalculator {

    int firstOperand;
    int secondOperand;

    public Сalculator(int firstOperand, int secondOperand) {
        this.firstOperand = firstOperand;
        this.secondOperand = secondOperand;
    }

    public Сalculator() {
    }

    public int getFirstOperand() {
        return firstOperand;
    }

    public void setFirstOperand(int firstOperand) {
        this.firstOperand = firstOperand;
    }

    public int getSecondOperand() {
        return secondOperand;
    }

    public void setSecondOperand(int secondOperand) {
        this.secondOperand = secondOperand;
    }

    public int getSumm (){

        return firstOperand + secondOperand;

    }
}
