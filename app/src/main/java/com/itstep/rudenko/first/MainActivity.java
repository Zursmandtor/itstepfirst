package com.itstep.rudenko.first;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    EditText firstOperand;
    EditText secondOperand;
    EditText result;
    Button buttonResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        firstOperand = (EditText) findViewById(R.id.firstOperand);
        secondOperand = (EditText) findViewById(R.id.secondOperand);
        result = (EditText) findViewById(R.id.buttonResult);
        buttonResult = (Button) findViewById(R.id.buttonResult);
    }
}